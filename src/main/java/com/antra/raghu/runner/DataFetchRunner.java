package com.antra.raghu.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.antra.raghu.repo.CityRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DataFetchRunner implements CommandLineRunner {

	@Autowired
	private CityRepository cityRepository;

	@Override
	public void run(String... args) throws Exception {

		var cities = cityRepository.findByNameEndingWithAndPopulationLessThan("est", 1800000);
		cities.forEach(city -> log.info("{}", city));
	}

}
