package com.antra.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaTestExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaTestExApplication.class, args);
	}

}
